import util

## MAC0425 - Inteligência Artificial
## Aluno: Rodrigo Volpe Battistin
## NUSP: 11795464

def h_naive(state, planning):
    return 0


def h_add(state, planning):
    '''
    Return heuristic h_add value for `state`.

    OBSERVATION: It receives `planning` object in order
    to access the applicable actions and problem information.
    '''
    ' YOUR CODE HERE '
    h = {}
    for atom in state:
        h[atom] = 0

    props = state
    changed = True
    while (changed):
        changed = False
        for action in planning.applicable(props):
            props = props.union(action.pos_effect)
            dists = 1 + sum(h.get(atom, float("Inf")) for atom in action.precond)

            for atom in action.pos_effect:
                if dists < h.get(atom, float("Inf")):
                    h[atom] = dists
                    changed = True

    return sum(h.get(atom, float("Inf")) for atom in planning.problem.goal)

def h_max(state, planning):
    '''
    Return heuristic h_max value for `state`.

    OBSERVATION: It receives `planning` object in order
    to access the applicable actions and problem information.
    '''
    ' YOUR CODE HERE '
    h = {}
    for atom in state:
        h[atom] = 0

    props = state
    changed = True
    while (changed):
        changed = False
        for action in planning.applicable(props):
            props = props.union(action.pos_effect)
            dists = 1 + sum(h.get(atom, float("Inf")) for atom in action.precond)

            for atom in action.pos_effect:
                if dists < h.get(atom, float("Inf")):
                    h[atom] = dists
                    changed = True

    return max(h.get(atom, float("Inf")) for atom in planning.problem.goal)

def h_ff(state, planning):
    '''
    Return heuristic h_ff value for `state`.

    OBSERVATION: It receives `planning` object in order
    to access the applicable actions and problem information.
    '''
    util.raiseNotDefined()
    ' YOUR CODE HERE '
