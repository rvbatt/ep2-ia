import util
from state import State

## MAC0425 - Inteligência Artificial
## Aluno: Rodrigo Volpe Battistin
## NUSP: 11795464

def validate(problem, solution):
    '''
    Return true if `solution` is a valid plan for `problem`.
    Otherwise, return false.

    OBSERVATION: you should check action applicability,
    next-state generation and if final state is indeed a goal state.
    It should give you some indication of the correctness of your planner,
    mainly for debugging purposes.
    '''
    ' YOUR CODE HERE '
    state = State(problem.init)

    for i in range(len(solution)):
        action = solution[i]
        
        if not action.precond.issubset(state):
            return False

        state = state.union(action.pos_effect).difference(action.neg_effect)

    return problem.goal.issubset(state)

